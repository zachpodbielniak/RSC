#!/bin/python3

'''
 ____  ____   ____ 
|  _ \/ ___| / ___|
| |_) \___ \| |    
|  _ < ___) | |___ 
|_| \_\____/ \____|

Remote Shell Call. Call Shells Remotely Over HTTP.
Copyright (C) 2020 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import subprocess
import sys
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from flask import Flask, request, render_template
from flask_restplus import Api, Resource, fields
from ApiToken import ApiToken
from Version import Version
import oneagent

# Set to False to disable OneAgent
oa_enabled = True
sdk = None
wappinfo = None

app = Flask(__name__)
api = Api(app)


# Tracer Decorator, allows easy instrumentation of each web request
def onagent_web(func):
	def tracer(*args, **kwargs):
		if oa_enabled:
			wreq = sdk.trace_incoming_web_request(
				wappinfo,
				request.base_url,
				request.method,
				remote_address=request.remote_addr,
				headers=dict(request.headers)
			)


		if wreq:
			with wreq:
				# Pass in wreq as a kwarg, so it can still be used for params and such
				kwargs['wreq'] = wreq 
				jRet, iStatus = func(*args, **kwargs)
				wreq.set_status_code(iStatus)
		else:
			jRet, iStatus = func(*args, **kwargs)

		return jRet, iStatus
	return tracer

	
@api.route('/health')
class Health(Resource):
	@onagent_web # Web Tracer Decorator
	def get(self, *args, **kwargs):
		wreq = kwargs['wreq']

		# Get The Token
		tkn1 = request.args.get("Api-Token")
		tkn2 = request.args.get("api-token")

		# Check For Token And Is Valid
		if tkn1 in ApiToken or tkn2 in ApiToken:
			if wreq:
				wreq.add_parameter("status", "healthy")
			return {"status": "healthy"}, 200

		if wreq:
			wreq.add_parameter("message", "Forbidden!")
		return {"message": "Forbidden!"}, 401




@app.route('/about')
def AboutGet():
	if oa_enabled:
		wreq = sdk.trace_incoming_web_request(
			wappinfo,
			request.base_url,
			request.method,
			remote_address=request.remote_addr,
			headers=dict(request.headers)
		)

		with wreq:
			wreq.set_status_code(200)
			
	return render_template('about.html', version=Version), 200, {'Content-Type': 'text/html; charset=utf-8'}
			



command = api.model("Command", {
	"command": fields.String(required=True, description="The shell command to run"),
	"format": fields.String(required=True, description="The output format type: text/plain or application/json")
})


@api.route('/run')
class Run(Resource):
	@onagent_web # Web Tracer Decorator
	def get(self, *args, **kwargs):
		wreq = kwargs['wreq']

		# Get The Token
		tkn1 = request.args.get("Api-Token")
		tkn2 = request.args.get("api-token")

		# Check For Token And Is Valid
		if tkn1 in ApiToken or tkn2 in ApiToken:
			return {"status": "healthy", "message": "Please use a POST"}, 200
		
		if wreq:
			with wreq:
				wreq.add_parameter("message", "Forbidden!")
		
		return {"message": "Forbidden!"}, 401


	@api.expect(command)
	@onagent_web # Web Tracer Decorator
	def post(self, *args, **kwargs):
		wreq = kwargs['wreq']
		# Get The Token
		tkn1 = request.args.get("Api-Token")
		tkn2 = request.args.get("api-token")

		# Check For Token And Is Valid
		if tkn1 in ApiToken or tkn2 in ApiToken:
			output = subprocess.Popen(
				api.payload['command'],
				shell=True,
				stdout=subprocess.PIPE
			).stdout.read()


			if wreq: # If oa_enabled
				with wreq:
					sdk.add_custom_request_attribute("output", output)
					wreq.add_parameter('command', api.payload['command'])
					wreq.add_parameter('format', api.payload['format'])


			if api.payload['format'] == "application/json":
				return {"output": output}, 200
			return output, 200
		
		if wreq:
			with wreq:
				wreq.add_parameter('message', 'Forbidden!')
		return {"message": "Forbidden!"}, 401





if __name__ == '__main__':
	if oa_enabled:
		if not oneagent.initialize():
			print("Could not initialize OneAgent SDK")
			sys.exit(1)
		sdk = oneagent.get_sdk()

		wappinfo = sdk.create_web_application_info(
			virtual_host='RSC',
			application_id='RemoteShellCall',
			context_root='/'
		)

	app.run(debug=True)
	if oa_enabled:
		oneagent.shutdown()
